<?php


function dbcon(){
	$host = "localhost";
	$user = "root";
	$pwd = "1234";
	$db = "db_elearning";

	$con = mysqli_connect($host,$user,$pwd,$db);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	return $con;
}

function dbclose(){
		
	mysqli_close(dbcon());
}

function topic(){
	dbcon();
	$sel = mysqli_query(dbcon(),"SELECT * from tbl_topic");

	if($sel==true){
		while($row=mysqli_fetch_assoc($sel)){
			//extract($row);
			echo '<option value='.$row['topic_Id'].'>'.$row['name'].'</option>';
		}
	}


	dbclose();
}

function subtopic(){
	dbcon();
	$sel = mysqli_query(dbcon(),"SELECT * from tbl_subtopic");

	if($sel==true){
		while($row=mysqli_fetch_assoc($sel)){
			//extract($row);
			echo '<option value='.$row['sub_Id'].'>'.$row['sub_title'].'</option>';
		}
	}


	dbclose();
}

?>