<!DOCTYPE html>
<html>
<head>
	  <title>Students Master List</title>
	    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
         <link rel="icon" type="image/png"  href="../../images/faviconi.png">
</head>
<body>
<div class="container" style="margin:5% auto;">
	 <button class='pull-right'onclick=window.print() id='print-button'>Print</button>
	 <h2>Student's Master List and Account Information</h2>
	  <table class="table table-striped table-bordered" id="example2">
                                        <thead>
                                        <tr>
                                           
                                            <th>Name</th>
                                            <th>Date of Birth</th>
                                            <th>Gender</th>
                                            <th>Course</th>
                                            <th>Class</th>
                                            <th>Parent</th>
                                         
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                      <?php
                                        
                                        include "../../functions/connect.php";
                                      
                                        $sql = "SELECT * FROM `tbl_student` ";
                                        $run = mysqli_query($con,$sql);

                                        while($row=mysqli_fetch_array($run)){
                                            $id = $row['user_Id'];
                                            echo '<tr class="odd gradeX" id="rec">';?>
                                           <?php
                                            echo "<td>".$row['fname']." ".$row['mname']." ".$row['lname']."</td>";
                                            echo "<td>".$row['dob']."</td>";
                                            echo "<td>".$row['gender']."</td>";
                                            echo "<td>".$row['course_Id']."</td>";
                                            echo "<td>".$row['class_Id']."</td>";
                                            echo "<td>".$row['parent_Id']."</td>";
                                    
                                            
        
                                            echo "</tr>";
                                 
                                     
                                        }
                                    

                                        ?>
                                   
                                        
                                    </tbody>
                                    </table>
</div>
  <style type="text/css">
@media print {
    #print-button {
        display :  none;
    }

}
</style>

</body>
</html>